/* -*- c++ -*-
  poly.h

  minimal polynomial class with just those operations
  needed for maxmod and its testing;

  this is C++11

  J.J. Green 2012
*/

#define VERBOSE_SF 12

#include <limits>
#include <vector>
#include <list>

template <class R, class C>
class poly
{
protected:
  std::vector<C> coefs;

public:
  // constructors
  poly(){};
  poly(std::vector<C> c){ coefs = c; };
  poly(std::initializer_list<C> c){ coefs = c; };

  // methods
  poly<R, C> conj();
  poly<R, C> operator*(poly<R, C>);

  C eval(C);
  C evalt(R);
  R abs2eval(C);
  R l1norm();
  R maxmod(R, bool);
  R maxmod(bool);
  R maxmod(R);
  R maxmod();

  // friend methods
  template <class R0, class C0>
  friend std::ostream& operator<< (std::ostream&, const poly<R0, C0>&);
};

// print (complex) coefficient of the polynomial

template <class R, class C>
std::ostream& operator << (std::ostream& st, const poly<R, C>& p)
{
  const std::vector<C> c = p.coefs;

  for (size_t i = 0 ; i < c.size() ; i++)
    st << c[i];

  return st;
}


// evaluate the polynomial at the complex value z,
// implemented with Horner's rule

template <class R, class C>
C poly<R, C>::eval(C z)
{
  size_t n = coefs.size() - 1;
  C sum;

  for (sum = coefs[n] ;
       n-- ;
       sum = sum * z + coefs[n]);

  return sum;
};

// evaluate at exp(it)

template <class R, class C>
C poly<R, C>::evalt(R t)
{
  C z  = C( std::cos(t), std::sin(t) );
  return this->eval(z);
}

// calculate |p(z)|^2

template <class R, class C>
R poly<R, C>::abs2eval(C z)
{
  C val = eval(z);
  R re = real(val), im = imag(val);

  return re*re + im*im;
}

// l1 norm of the polynomial is the sum of absolute
// values of the coefficients, implemented here using
// the Kahane summation formula

template <class R, class C>
R poly<R, C>::l1norm()
{
  size_t n = coefs.size();
  R sum = 0.0, e = 0.0;

  for (size_t i=0 ; i<n ; i++)
    {
      R tmp = sum;
      R y = abs(coefs[i]) + e;

      sum = tmp + y;
      e = (tmp - sum) + y;
    }

  return sum;
}

// conjugate of a polynomial

template <class R, class C>
poly<R, C> poly<R, C>::conj()
{
  int n = coefs.size();
  std::vector<C> q(n);

  for (int i=0 ; i<n ; i++)
    q[n-i-1] = std::conj(coefs[i]);

  return poly<R, C>(q);
}

// multiply polynomials (simple minded n^2 method)

template <class R,class C>
poly<R, C> poly<R, C>::operator*(poly<R, C> p)
{
  std::vector<C>
    a = coefs,
    b = p.coefs,
    c(a.size() + b.size() - 1);

  for (size_t i=0 ; i<a.size() ; i++)
    {
      for (size_t j=0 ; j<b.size() ; j++)
	{
	  c[i+j] += a[i]*b[j];
	}
    }

  return poly<R, C>(c);
}

// utility struct containing the theta (t) of the
// midpoint of the interval and q = |p(exp(it))|^2

template <class R>
struct interval { R t, q; };

// maximum modulus

template <class R, class C>
R poly<R,C>::maxmod(R eps, bool verbose)
{
  const R inf = std::numeric_limits<R>::infinity();
  const R pi = atan((R)1) * 4;

  const size_t N = this->coefs.size(), M = 5 * N;
  poly<R, C> beta = (*this) * (this->conj());
  R beta0 = std::real(beta.coefs[N - 1]);
  R normq = beta.l1norm();

  // check for near a monomial

  if (normq - beta0 < 4 * eps * beta0)
    return std::sqrt( (beta0 + normq) / 2 );

  // create initial list of values

  R h = pi / M;
  std::list< interval<R> > L;

  for (size_t i = 0 ; i < M ; i++)
    {
      R t = 2 * h * i + h;
      interval<R> I = { t, std::norm( this->evalt(t) ) };
      L.push_front(I);
    }

  // setup for the main loop

  typename std::list< interval<R> >::iterator it;
  size_t eval0 = M, evals = M;
  R M2min = 0, M2max = inf;
  R A = std::max(2*beta0-normq,(R)0);

  if (verbose)
    printf("lower bound\tupper bound\tevals\trejected\n");

  while (1)
    {
      // find maximum value of interval midpoints, which
      // gives us a lower bound on M2

      for (it = L.begin() ; it != L.end() ; it++)
	M2min = std::max(M2min, it->q);

      // upper bound on M2 from Steckin's lemma

      R SC = cos(N * h);
      M2max = (M2min - A) / SC + A;

      // check for termination

      if (M2max - M2min < 4 * eps * M2min)
	{
	  if (verbose)
	    printf("%.*f\t%.*f\t%zi\t-\n",
		   VERBOSE_SF, sqrt(M2min),
		   VERBOSE_SF, sqrt(M2max),
		   eval0);
	  break;
	}

      // rejection

      R rT = (M2min - A) * SC + A;
      size_t nrej = 0;

      it = L.begin();
      while (it != L.end())
	{
	  if (it->q < rT)
	    {
	      L.erase(it++);
	      nrej++;
	    }
	  else
	    it++;
	}

      if (verbose)
	printf("%.*f\t%.*f\t%zi\t%zi\n",
	       VERBOSE_SF, sqrt(M2min),
	       VERBOSE_SF, sqrt(M2max),
	       eval0, nrej);

      // subdivide

      h /= 3;
      eval0 = 0;

      for (it = L.begin() ; it != L.end() ; it++)
	{
	  R t[2] = { it->t + 2 * h, it->t - 2 * h };

	  for (int i = 0 ; i < 2 ; i++)
	    {
	      interval<R> I = { t[i], std::norm( this->evalt(t[i]) ) };
	      L.push_front(I);
	    }

	  eval0 += 2;
	}

      evals += eval0;
    }

  if (verbose)
    printf("evaluations: %zi\n", evals);

  return sqrt((M2min + M2max) / 2);
}

// versions with default verbosity and precision

#define DMEPS 10
#define DVERB false

template <class R, class C>
R poly<R, C>::maxmod(R eps)
{
  return this->maxmod(eps, DVERB);
}

template <class R, class C>
R poly<R, C>::maxmod(bool verbose)
{
  const R eps = DMEPS * std::numeric_limits<R>::epsilon();
  return this->maxmod(eps, verbose);
}

template <class R, class C>
R poly<R,C>::maxmod()
{
  const R eps = DMEPS * std::numeric_limits<R>::epsilon();
  return this->maxmod(eps, DVERB);
}
