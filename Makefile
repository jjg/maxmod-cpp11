# Makefile for maxmod++

VERSION = 1.0
RUBBISH = minimal testsuite *~
CXXFLAGS = -std=c++0x -Wall -O3

minimal: minimal.cc poly.h
	$(CXX) $(CXXFLAGS) -o minimal minimal.cc

testsuite: testsuite.cc poly.h
	$(CXX) $(CXXFLAGS) -o testsuite testsuite.cc

test check: testsuite
	./testsuite

clean:
	$(RM) $(RUBBISH)

PKGNAME = maxmodpp-$(VERSION)
PKG = $(PKGNAME).tar.gz
PKGFILES = poly.h minimal.cc testsuite.cc Makefile \
           README COPYING CHANGES
RUBBISH += $(PKG)
PKGDIR = ${JJGSOURCE}/src/www/sites/jjg/tt/src/code/

pkg: $(PKG)

$(PKG): $(PKGFILES)
	install -d $(PKGNAME)
	for file in $(PKGFILES) ; do \
	  install -m 644 $$file $(PKGNAME)/ ; \
	done
	tar -zcvf $(PKG) $(PKGNAME)
	for file in $(PKGFILES) ; do \
	  rm $(PKGNAME)/$$file ; \
	done
	rmdir $(PKGNAME)

install-pkg: $(PKG)
	for file in $(PKG) ; do \
	  install -m644 $$file $(PKGDIR) ; \
	done
