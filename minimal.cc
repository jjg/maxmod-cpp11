/*
  minimal.cc

  minimal example of use of maxmod in the poly class

  J.J. Green 2012
*/

#include <iostream>
#include <complex>

#include "poly.h"

typedef long double real_t;
typedef std::complex<real_t> complex_t;
typedef poly<real_t, complex_t> poly_t;

int main(void)
{
  poly_t p = {1, 0, 0, 1};
  bool verbose = true;

  std::cout 
    << "polynomial " << p
    << " -> " << p.maxmod(verbose) 
    << std::endl;

  return 0;
}


